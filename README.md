# API Clientes/Produtos Favoritos

Api construída como parte do processo seletivo para o Luizalabs

## Tecnologias utilizadas

- NestJS
- MongoDB
- Docker
- Swagger

## Iniciando aplicação

### 1 - preencher o .env com os valores pertinentes se baseando nas chaves existentes em .env.example ### 
<br />

MONGODB_CONNECTION_STRING = `url de conexão com o banco - mongodb://mongo:27017`

MONGODB_DATABASE = `nome do banco`

API_PRODUTOS_URL= `URL da api de produtos fornecida no arquivo descritivo do teste técnico`

SECRET = `Secret a ser utilizado no JWT`


### 2 - executar na raiz do projeto o comando para que o docker construa e inicie os containers

``` 
docker-compose up -d
```

### 3 - Acessar o swagger com os endpoints disponíveis no seguinte endereço

```
localhost:3000/swagger-ui
```

### 4 - Gerar token para ter acesso às rotas protegidas

Faça uma requisição para a rota `/auth/login` com o body 

```json
  {
    "username": "admin",
    "password": "123456"
  }
```

utilize o token retornado na opção `Authorize` no topo da página do swagger

## Testando a aplicação

para executar os testes implementados na aplicação é necessário que se execute o comando

```
yarn test
```
