import { HttpService, HttpStatus, Injectable } from '@nestjs/common';
import { Produto } from './entities/produto.entity';

@Injectable()
export class ProdutoService {
  constructor(private httpService: HttpService) {}

  async findProduto(id: string): Promise<Produto> {
    return this.httpService
      .get(`${process.env.API_PRODUTOS_URL}/${id}/`)
      .toPromise()
      .then((res) => {
        if (res.data.products) {
          const produtoEncontrado = res.data.products.find(
            (prod) => prod.id === id,
          );

          return produtoEncontrado;
        }

        return res.data;
      })
      .catch((err) => {
        if (err.response.status === HttpStatus.NOT_FOUND) return null;
        throw err;
      });
  }
}
