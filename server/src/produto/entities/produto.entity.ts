import { Column } from 'typeorm';

export class Produto {
  @Column() price: number;
  @Column() image: string;
  @Column() brand: string;
  @Column() id: string;
  @Column() title: string;
  @Column() reviewScore?: number;

  constructor(produto?: Partial<Produto>) {
    Object.assign(this, produto);
  }
}
