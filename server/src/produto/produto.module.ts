import { Module } from '@nestjs/common';
import { ProdutoService } from './produto.service';
import { HttpModule } from '@nestjs/common';

@Module({
  imports: [HttpModule],
  providers: [ProdutoService],
  exports: [ProdutoService],
})
export class ProdutoModule {}
