import { HttpModule, HttpService } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { AxiosResponse } from 'axios';
import { of } from 'rxjs';
import { Produto } from './entities/produto.entity';
import { ProdutoService } from './produto.service';

const produtos: Array<Produto> = [
  {
    price: 1699.0,
    image:
      'http://challenge-api.luizalabs.com/images/1bf0f365-fbdd-4e21-9786-da459d78dd1f.jpg',
    brand: 'b\u00e9b\u00e9 confort',
    id: '1',
    title: 'Cadeira para Auto Iseos B\u00e9b\u00e9 Confort Earth Brown',
  },
  {
    price: 1149.0,
    image:
      'http://challenge-api.luizalabs.com/images/958ec015-cfcf-258d-c6df-1721de0ab6ea.jpg',
    brand: 'b\u00e9b\u00e9 confort',
    id: '2',
    title: 'Mois\u00e9s Dorel Windoo 1529',
  },
  {
    price: 1149.0,
    image:
      'http://challenge-api.luizalabs.com/images/6a512e6c-6627-d286-5d18-583558359ab6.jpg',
    brand: 'b\u00e9b\u00e9 confort',
    id: '3',
    title: 'Mois\u00e9s Dorel Windoo 1529',
  },
  {
    price: 1999.0,
    image:
      'http://challenge-api.luizalabs.com/images/4bd442b1-4a7d-2475-be97-a7b22a08a024.jpg',
    brand: 'b\u00e9b\u00e9 confort',
    id: '4',
    title: 'Cadeira para Auto Axiss B\u00e9b\u00e9 Confort Robin Red',
  },
];

describe('ProdutoService', () => {
  let service: ProdutoService;
  let httpService: HttpService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [HttpModule],
      providers: [ProdutoService],
    }).compile();

    service = module.get<ProdutoService>(ProdutoService);
    httpService = module.get<HttpService>(HttpService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('deve retornar um produto quando é passado um id válido', async () => {
    const dados: AxiosResponse = {
      data: {
        products: produtos,
      },
      status: 200,
      statusText: 'OK',
      headers: {},
      config: {},
    };
    jest.spyOn(httpService, 'get').mockImplementationOnce(() => of(dados));

    const ID_PRODUTO = '1';
    const EXPECTED_TITLE =
      'Cadeira para Auto Iseos B\u00e9b\u00e9 Confort Earth Brown';

    const result = await service.findProduto(ID_PRODUTO);

    expect(result).toBeDefined();
    expect(result).toHaveProperty('id');
    expect(result).toHaveProperty('title');
    expect(result['id']).toBe(ID_PRODUTO);
    expect(result['title']).toBe(EXPECTED_TITLE);
  });

  //it('deve retornar vazio quando o id passado é inválido', async () => {});
});
