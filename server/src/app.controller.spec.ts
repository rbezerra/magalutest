import { Test, TestingModule } from '@nestjs/testing';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthService } from './auth/auth.service';
import { UserDto } from './auth/dto/user.dto';

class AuthServiceMock {
  async login(user: UserDto): Promise<any> {
    return Promise.resolve({
      token:
        'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxIiwibmFtZSI6ImFkbWluIn0.VGfl2KKdkJQS4rXTMH7NIETD5OYMRIzxm4H5MIWidiI',
    });
  }
}

describe('AppController', () => {
  let appController: AppController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [AppController],
      providers: [
        AppService,
        {
          provide: AuthService,
          useClass: AuthServiceMock,
        },
      ],
    }).compile();

    appController = app.get<AppController>(AppController);
  });

  describe('root', () => {
    it('should return "Hello World!"', () => {
      expect(appController.getHello()).toBe('Hello World!');
    });

    it('Deve retornar um token ao efetuar login', async () => {
      const user: UserDto = {
        username: 'teste',
        password: '123456',
      };

      const result = await appController.login(user);

      expect(result).toBeDefined();
      expect(result).toHaveProperty('token');
      expect(result['token'].length).toBeGreaterThan(0);
    });
  });
});
