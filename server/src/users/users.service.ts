import { Injectable } from '@nestjs/common';
import { UserDto } from 'src/auth/dto/user.dto';

@Injectable()
export class UsersService {
  private readonly users: UserDto[] = [
    {
      id: 1,
      username: 'admin',
      password: '123456',
    },
    {
      id: 2,
      username: 'admin2',
      password: '123456',
    },
  ];

  async findOne(username: string): Promise<UserDto | undefined> {
    return this.users.find((user) => user.username === username);
  }
}
