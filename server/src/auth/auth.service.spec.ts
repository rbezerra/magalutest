import { JwtService } from '@nestjs/jwt';
import { Test, TestingModule } from '@nestjs/testing';
import { UsersService } from './../users/users.service';
import { AuthService } from './auth.service';
import { UserDto } from './dto/user.dto';

class UsersServiceMock {
  async findOne(_: string): Promise<UserDto> {
    return Promise.resolve({
      id: 1,
      username: 'admin',
      password: '1234',
    });
  }
}

class JwtServiceMock {
  sign(_: any): string {
    return 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxIiwibmFtZSI6ImFkbWluIn0.VGfl2KKdkJQS4rXTMH7NIETD5OYMRIzxm4H5MIWidiI';
  }
}

describe('AuthService', () => {
  let service: AuthService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AuthService,
        {
          provide: UsersService,
          useClass: UsersServiceMock,
        },
        {
          provide: JwtService,
          useClass: JwtServiceMock,
        },
      ],
    }).compile();

    service = module.get<AuthService>(AuthService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('deve retornar usuário quando o username e senha forem válidos', async () => {
    const USER: UserDto = {
      username: 'admin',
      password: '1234',
    };

    const result = await service.validateUser(USER.username, USER.password);

    expect(result).toBeDefined();
    expect(result).toHaveProperty('id');
    expect(result).toHaveProperty('username');
    expect(result['username']).toBe('admin');
    expect(result['id']).toBe(1);
  });

  it('deve retornar nulo quando o username e senha forem inválidos', async () => {
    const USER: UserDto = {
      username: 'admin',
      password: '123456',
    };

    const result = await service.validateUser(USER.username, USER.password);

    expect(result).toBeDefined();
    expect(result).toBeNull();
  });

  it('deve retornar um token ao efetuar login', async () => {
    const USER: UserDto = {
      id: 1,
      username: 'admin',
      password: '123456',
    };
    const result = await service.login(USER);

    expect(result).toBeDefined();
    expect(result).toHaveProperty('token');
    expect(typeof result['token']).toBe('string');
  });
});
