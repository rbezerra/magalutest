import { Body, Controller, Get, Post, UseGuards } from '@nestjs/common';
import { ApiResponse, ApiTags } from '@nestjs/swagger';
import { AppService } from './app.service';
import { AuthService } from './auth/auth.service';
import { UserDto } from './auth/dto/user.dto';
import { LocalAuthGuard } from './auth/local-auth.guard';

@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
    private authService: AuthService,
  ) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @UseGuards(LocalAuthGuard)
  @ApiTags('auth')
  @ApiResponse({
    status: 200,
    description: 'Autenticado com sucesso',
  })
  @ApiResponse({
    status: 401,
    description: 'Falha na autenticação',
  })
  @Post('auth/login')
  async login(@Body() req: UserDto) {
    return this.authService.login(req);
  }
}
