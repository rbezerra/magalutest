import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseFilters,
  HttpException,
  HttpStatus,
  UseGuards,
} from '@nestjs/common';
import { ApiBearerAuth, ApiResponse, ApiTags } from '@nestjs/swagger';
import { JwtAuthGuard } from './../auth/jwt-auth.guard';
import { ProdutoService } from './../produto/produto.service';
import { ClienteService } from './cliente.service';
import { CreateClienteDto } from './dto/create-cliente.dto';
import { UpdateClienteDto } from './dto/update-cliente.dto';
import { MongoExceptionFilter } from './filters/mongodb-exception.filter';

@UseGuards(JwtAuthGuard)
@ApiBearerAuth()
@Controller('cliente')
export class ClienteController {
  constructor(
    private readonly clienteService: ClienteService,
    private readonly produtoService: ProdutoService,
  ) {}

  @Post()
  @ApiResponse({
    status: 201,
    description: 'Cliente criado com sucesso',
  })
  @ApiResponse({
    status: 401,
    description: 'Não Autorizado',
  })
  @ApiTags('cliente')
  @UseFilters(MongoExceptionFilter)
  create(@Body() createClienteDto: CreateClienteDto) {
    return this.clienteService.create(createClienteDto);
  }

  @Get()
  @ApiResponse({
    status: 200,
    description: 'Lista dos clientes cadastrados',
  })
  @ApiResponse({
    status: 401,
    description: 'Não Autorizado',
  })
  @ApiTags('cliente')
  findAll() {
    return this.clienteService.findAll();
  }

  @Get(':id')
  @ApiResponse({
    status: 200,
    description: 'Dados do cliente',
  })
  @ApiResponse({
    status: 404,
    description: 'Cliente não encontrado',
  })
  @ApiResponse({
    status: 401,
    description: 'Não Autorizado',
  })
  @ApiTags('cliente')
  findOne(@Param('id') id: string) {
    return this.clienteService.findOne(id);
  }

  @Patch(':id')
  @ApiResponse({
    status: 200,
    description: 'Dados do cliente atualizados',
  })
  @ApiResponse({
    status: 404,
    description: 'Cliente não encontrado',
  })
  @ApiResponse({
    status: 401,
    description: 'Não Autorizado',
  })
  @ApiTags('cliente')
  @UseFilters(MongoExceptionFilter)
  update(@Param('id') id: string, @Body() updateClienteDto: UpdateClienteDto) {
    return this.clienteService.update(id, updateClienteDto);
  }

  @ApiResponse({
    status: 200,
    description: 'Dados do cliente atualizados',
  })
  @ApiResponse({
    status: 404,
    description: 'Cliente não encontrado',
  })
  @ApiResponse({
    status: 401,
    description: 'Não Autorizado',
  })
  @ApiTags('cliente')
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.clienteService.remove(id);
  }

  @Post(':idCliente/adicionar-produto/:idProduto')
  @ApiResponse({
    status: 201,
    description: 'Produto adicionado',
  })
  @ApiResponse({
    status: 404,
    description: 'Cliente não encontrado',
  })
  @ApiResponse({
    status: 401,
    description: 'Não Autorizado',
  })
  @ApiTags('produto')
  @UseFilters(MongoExceptionFilter)
  async adicionarProduto(
    @Param('idCliente') idCliente: string,
    @Param('idProduto') idProduto: string,
  ) {
    const produto = await this.produtoService.findProduto(idProduto);

    if (!produto)
      throw new HttpException('Produto não encontrado', HttpStatus.NOT_FOUND);

    this.clienteService.adicionarProduto(idCliente, produto);
  }

  @Post(':idCliente/remover-produto/:idProduto')
  @ApiResponse({
    status: 201,
    description: 'Produto removido',
  })
  @ApiResponse({
    status: 404,
    description: 'Cliente não encontrado',
  })
  @ApiResponse({
    status: 401,
    description: 'Não Autorizado',
  })
  @ApiTags('produto')
  @UseFilters(MongoExceptionFilter)
  async removerProduto(
    @Param('idCliente') idCliente: string,
    @Param('idProduto') idProduto: string,
  ) {
    const produto = await this.produtoService.findProduto(idProduto);

    if (!produto)
      throw new HttpException('Produto não encontrado', HttpStatus.NOT_FOUND);

    this.clienteService.removerProduto(idCliente, idProduto);
  }
}
