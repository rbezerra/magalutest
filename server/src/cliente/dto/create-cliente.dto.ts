import { ApiProperty } from '@nestjs/swagger';

export class CreateClienteDto {
  @ApiProperty() nome: string;
  @ApiProperty() email: string;
}
