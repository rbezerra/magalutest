import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Produto } from 'src/produto/entities/produto.entity';
import { DeleteResult, MongoRepository, UpdateResult } from 'typeorm';
import { CreateClienteDto } from './dto/create-cliente.dto';
import { UpdateClienteDto } from './dto/update-cliente.dto';
import { Cliente } from './entities/cliente.entity';

@Injectable()
export class ClienteService {
  constructor(
    @InjectRepository(Cliente)
    private readonly clientesRepository: MongoRepository<Cliente>,
  ) {}

  async create(createClienteDto: CreateClienteDto): Promise<Cliente> {
    return this.clientesRepository.save(createClienteDto);
  }

  async findAll(): Promise<Cliente[]> {
    return this.clientesRepository.find();
  }

  async findOne(id: string): Promise<Cliente> {
    return this.clientesRepository.findOne(id);
  }

  async update(
    id: string,
    updateClienteDto: UpdateClienteDto,
  ): Promise<UpdateResult> {
    const cliente = await this.clientesRepository.findOne(id);
    if (!cliente) {
      throw new NotFoundException();
    }

    return this.clientesRepository.update(cliente.id, updateClienteDto);
  }

  async remove(id: string): Promise<DeleteResult> {
    const cliente = await this.clientesRepository.findOne(id);
    if (!cliente) {
      throw new NotFoundException();
    }

    return this.clientesRepository.delete(cliente.id);
  }

  async adicionarProduto(
    idCliente: string,
    produto: Produto,
  ): Promise<UpdateResult> {
    const cliente = await this.clientesRepository.findOne(idCliente);
    if (!cliente) {
      throw new NotFoundException();
    }

    if (!cliente.favoritos) cliente.favoritos = new Array<Produto>();

    cliente.favoritos.push(produto);

    return this.clientesRepository.update(cliente.id, cliente);
  }

  async removerProduto(
    idCliente: string,
    idProduto: string,
  ): Promise<UpdateResult> {
    const cliente = await this.clientesRepository.findOne(idCliente);

    if (!cliente) {
      throw new NotFoundException();
    }

    if (!cliente.favoritos) cliente.favoritos = new Array<Produto>();

    cliente.favoritos = cliente.favoritos.filter(
      (prod) => prod.id !== idProduto,
    );

    return this.clientesRepository.update(cliente.id, cliente);
  }
}
