import { HttpException, HttpStatus, NotFoundException } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { Produto } from 'src/produto/entities/produto.entity';
import { ObjectID, UpdateResult } from 'typeorm';
import { ProdutoService } from './../produto/produto.service';
import { ClienteController } from './cliente.controller';
import { ClienteService } from './cliente.service';
import { CreateClienteDto } from './dto/create-cliente.dto';
import { UpdateClienteDto } from './dto/update-cliente.dto';
import { Cliente } from './entities/cliente.entity';

const _produtos: Produto[] = [
  {
    price: 1699.0,
    image:
      'http://challenge-api.luizalabs.com/images/1bf0f365-fbdd-4e21-9786-da459d78dd1f.jpg',
    brand: 'b\u00e9b\u00e9 confort',
    id: '1',
    title: 'Cadeira para Auto Iseos B\u00e9b\u00e9 Confort Earth Brown',
  },
  {
    price: 1149.0,
    image:
      'http://challenge-api.luizalabs.com/images/958ec015-cfcf-258d-c6df-1721de0ab6ea.jpg',
    brand: 'b\u00e9b\u00e9 confort',
    id: '2',
    title: 'Mois\u00e9s Dorel Windoo 1529',
  },
  {
    price: 1149.0,
    image:
      'http://challenge-api.luizalabs.com/images/6a512e6c-6627-d286-5d18-583558359ab6.jpg',
    brand: 'b\u00e9b\u00e9 confort',
    id: '3',
    title: 'Mois\u00e9s Dorel Windoo 1529',
  },
  {
    price: 1999.0,
    image:
      'http://challenge-api.luizalabs.com/images/4bd442b1-4a7d-2475-be97-a7b22a08a024.jpg',
    brand: 'b\u00e9b\u00e9 confort',
    id: '4',
    title: 'Cadeira para Auto Axiss B\u00e9b\u00e9 Confort Robin Red',
  },
];
class ClienteServiceMock {
  produtos = _produtos;
  clientes: Array<Cliente> = [
    {
      id: ('e9a72482-7e95-44ff-ea5a-75147aef2181' as unknown) as ObjectID,
      nome: 'Teste',
      email: 'teste@teste.com',
      favoritos: [],
    },
    {
      id: ('e9a72482-7e95-44ff-ea5a-75147aef2182' as unknown) as ObjectID,
      nome: 'Teste2',
      email: 'teste2@teste.com',
      favoritos: [],
    },
    {
      id: ('e9a72482-7e95-44ff-ea5a-75147aef2183' as unknown) as ObjectID,
      nome: 'Teste3',
      email: 'teste3@teste.com',
      favoritos: [],
    },
  ];

  async create(createClienteDto: CreateClienteDto): Promise<Cliente> {
    const novoCliente: Cliente = {
      id: ('999' as unknown) as ObjectID,
      favoritos: [],
      ...createClienteDto,
    };
    this.clientes.push(novoCliente);

    return Promise.resolve(novoCliente);
  }

  async findAll(): Promise<Array<Cliente>> {
    return Promise.resolve(this.clientes);
  }

  async findOne(id: string): Promise<Cliente> {
    return Promise.resolve(this.clientes.find((c) => c.id.toString() === id));
  }

  async update(id: string, updateClienteDto: UpdateClienteDto): Promise<any> {
    const index = this.clientes.findIndex((c) => c.id.toString() === id);

    if (index < 0) throw new NotFoundException();

    this.clientes[index] = { ...this.clientes[index], ...updateClienteDto };

    return Promise.resolve(this.clientes[index]);
  }

  async remove(id: string): Promise<boolean> {
    this.clientes = this.clientes.filter((c) => c.id.toString() !== id);
    return Promise.resolve(true);
  }

  async adicionarProduto(
    idCliente: string,
    produto: Produto,
  ): Promise<boolean> {
    const index = this.clientes.findIndex((c) => c.id.toString() === idCliente);

    this.clientes[index].favoritos.push(produto);

    return Promise.resolve(true);
  }

  async removerProduto(
    idCliente: 'string',
    idProduto: 'string',
  ): Promise<boolean> {
    const index = this.clientes.findIndex((c) => c.id.toString() === idCliente);

    this.clientes[index].favoritos = this.clientes[index].favoritos.filter(
      (p) => p.id.toString() !== idProduto,
    );

    return Promise.resolve(true);
  }
}

class ProdutoServiceMock {
  produtos = _produtos;

  async findProduto(id: string): Promise<Produto> {
    return Promise.resolve(this.produtos.find((p) => p.id === id));
  }
}

describe('ClienteController', () => {
  let controller: ClienteController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ClienteController],
      providers: [
        {
          provide: ClienteService,
          useClass: ClienteServiceMock,
        },
        {
          provide: ProdutoService,
          useClass: ProdutoServiceMock,
        },
      ],
    }).compile();

    controller = module.get<ClienteController>(ClienteController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('deve criar um novo cliente', async () => {
    const input: CreateClienteDto = {
      nome: 'novo',
      email: 'novo@novo.com',
    };

    const result = await controller.create(input);

    expect(result).toBeDefined();
    expect(result).toHaveProperty('id');
    expect(result).toHaveProperty('favoritos');
    expect(result['id']).toBe('999');
    expect(result.nome).toBe(input.nome);
  });

  it('deve listar todos os clientes', async () => {
    const result = await controller.findAll();

    expect(result).toBeDefined();
    expect(Array.isArray(result)).toBeTruthy();
    expect(result.length).toBeGreaterThan(0);
  });

  it('deve retornar um cliente quando é passado um id válido', async () => {
    const ID = 'e9a72482-7e95-44ff-ea5a-75147aef2181';

    const result = await controller.findOne(ID);
    expect(result).toBeDefined();
    expect(result).toHaveProperty('id');
    expect(result).toHaveProperty('nome');
    expect(result['id']).toBe(ID);
    expect(result['nome']).toBe('Teste');
  });

  it('deve retornar nulo quando é passado um id inválido', async () => {
    const ID = '999';

    const result = await controller.findOne(ID);
    expect(result).toBeUndefined();
  });

  it('deve atualizar um cliente quando é passado um id válido', async () => {
    const ID = 'e9a72482-7e95-44ff-ea5a-75147aef2181';

    const atualizacaoCliente: UpdateClienteDto = {
      nome: 'NovoNome',
    };

    const result = await controller.update(ID, atualizacaoCliente);

    expect(result).toBeDefined();
    expect(result).toHaveProperty('nome');
    expect(result['nome']).toBe(atualizacaoCliente.nome);
  });

  it('deve retornar erro quando é passado um id inválido', async () => {
    const ID = '9999';

    const atualizacaoCliente: UpdateClienteDto = {
      nome: 'NovoNome',
    };

    await expect(controller.update(ID, atualizacaoCliente)).rejects.toEqual(
      new NotFoundException(),
    );
  });

  it('deve remover um usuário quando for enviado um id válido', async () => {
    const ID = 'e9a72482-7e95-44ff-ea5a-75147aef2181';

    const result = await controller.remove(ID);

    expect(result).toBeDefined();
    expect(result).toBeTruthy();

    const clientes = await controller.findAll();
    expect(clientes).toBeDefined();
    expect(clientes.length).toBe(2);
  });

  it('deve manter a mesma quantidade de registros quando for enviado um id inválido', async () => {
    const ID = '999';

    const result = await controller.remove(ID);

    expect(result).toBeDefined();
    expect(result).toBeTruthy();

    const clientes = await controller.findAll();
    expect(clientes).toBeDefined();
    expect(clientes.length).toBe(3);
  });

  it('deve adicionar um produto quando for enviada um id válido', async () => {
    const ID_PRODUTO = '1';
    const ID_CLIENTE = 'e9a72482-7e95-44ff-ea5a-75147aef2181';

    await controller.adicionarProduto(ID_CLIENTE, ID_PRODUTO);

    const cliente = await controller.findOne(ID_CLIENTE);

    expect(cliente).toBeDefined();
    expect(cliente).toHaveProperty('favoritos');
    expect(cliente['favoritos'].length).toBe(1);
    expect(cliente['favoritos'][0].id.toString()).toBe(ID_PRODUTO);
  });

  it('deve retornar um erro ao adicionar produto com um id inválido', async () => {
    const ID_PRODUTO = '9';
    const ID_CLIENTE = 'e9a72482-7e95-44ff-ea5a-75147aef2181';

    await expect(
      controller.adicionarProduto(ID_CLIENTE, ID_PRODUTO),
    ).rejects.toEqual(
      new HttpException('Produto não encontrado', HttpStatus.NOT_FOUND),
    );
  });

  it('deve remover um produto quando for enviado um id válido', async () => {
    const ID_PRODUTO = '1';
    const ID_CLIENTE = 'e9a72482-7e95-44ff-ea5a-75147aef2181';

    await controller.adicionarProduto(ID_CLIENTE, ID_PRODUTO);
    await controller.removerProduto(ID_CLIENTE, ID_PRODUTO);

    const cliente = await controller.findOne(ID_CLIENTE);

    expect(cliente).toBeDefined();
    expect(cliente).toHaveProperty('favoritos');
    expect(cliente['favoritos'].length).toBe(0);
  });

  it('deve retornar um erro ao remover produto com um id inválido', async () => {
    const ID_PRODUTO = '9';
    const ID_CLIENTE = 'e9a72482-7e95-44ff-ea5a-75147aef2181';

    await expect(
      controller.removerProduto(ID_CLIENTE, ID_PRODUTO),
    ).rejects.toEqual(
      new HttpException('Produto não encontrado', HttpStatus.NOT_FOUND),
    );
  });
});
