import { ArgumentsHost, Catch, ExceptionFilter } from '@nestjs/common';
import { Response } from 'express';
import { MongoError } from 'mongodb';

@Catch(MongoError)
export class MongoExceptionFilter implements ExceptionFilter {
  catch(exception: MongoError, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();

    let responseObj: Object;
    let code: number;

    switch (exception.code) {
      case 11000:
        code = 409;
        responseObj = {
          status: 'error',
          message: 'email já cadastrado',
        };
        break;
      default:
        code = 500;
        responseObj = {
          status: 'error',
          message: exception.message,
        };
    }

    response.status(code).json(responseObj);
  }
}
