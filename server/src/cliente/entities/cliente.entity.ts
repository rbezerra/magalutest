import { Produto } from './../../produto/entities/produto.entity';
import { Column, Entity, ObjectID, ObjectIdColumn, Unique } from 'typeorm';

@Entity('clientes')
@Unique(['email'])
export class Cliente {
  @ObjectIdColumn() id: ObjectID;
  @Column() nome: string;

  @Column({ unique: true })
  email: string;

  @Column((type) => Produto)
  favoritos: Produto[];

  constructor(cliente?: Partial<Cliente>) {
    Object.assign(this, cliente);
  }
}
