import { Test, TestingModule } from '@nestjs/testing';
import { Produto } from './../produto/entities/produto.entity';
import { ProdutoService } from './../produto/produto.service';
import { ObjectID } from 'typeorm';
import { ClienteService } from './cliente.service';
import { CreateClienteDto } from './dto/create-cliente.dto';
import { Cliente } from './entities/cliente.entity';
import { NotFoundException } from '@nestjs/common';

class MongoRepositoryMock {
  clientes: Cliente[] = [
    {
      id: ('e9a72482-7e95-44ff-ea5a-75147aef2181' as unknown) as ObjectID,
      nome: 'Teste',
      email: 'teste@teste.com',
      favoritos: [],
    },
    {
      id: ('e9a72482-7e95-44ff-ea5a-75147aef2182' as unknown) as ObjectID,
      nome: 'Teste2',
      email: 'teste2@teste.com',
      favoritos: [],
    },
    {
      id: ('e9a72482-7e95-44ff-ea5a-75147aef2183' as unknown) as ObjectID,
      nome: 'Teste3',
      email: 'teste3@teste.com',
      favoritos: [],
    },
  ];

  async save(createClienteDto: CreateClienteDto): Promise<Cliente> {
    const cliente: Cliente = {
      id: ('999' as unknown) as ObjectID,
      nome: createClienteDto.nome,
      email: createClienteDto.email,
      favoritos: [],
    };
    return Promise.resolve(cliente);
  }

  async find(): Promise<Cliente[]> {
    return Promise.resolve(this.clientes);
  }

  async findOne(id: string): Promise<Cliente | undefined> {
    return Promise.resolve(
      this.clientes.find((cliente) => cliente.id.toString() === id),
    );
  }

  async update(
    id: string,
    clienteAtualizado: Cliente,
  ): Promise<Cliente | undefined> {
    const index = this.clientes.findIndex((c) => c.id.toString() === id);

    this.clientes[index] = clienteAtualizado;

    return Promise.resolve(this.clientes[index]);
  }

  async delete(id: string): Promise<boolean> {
    this.clientes = this.clientes.filter((c) => c.id.toString() !== id);
    return Promise.resolve(true);
  }
}

class ProdutoServiceMock {
  produtos: Produto[] = [
    {
      price: 1699.0,
      image:
        'http://challenge-api.luizalabs.com/images/1bf0f365-fbdd-4e21-9786-da459d78dd1f.jpg',
      brand: 'b\u00e9b\u00e9 confort',
      id: '1',
      title: 'Cadeira para Auto Iseos B\u00e9b\u00e9 Confort Earth Brown',
    },
    {
      price: 1149.0,
      image:
        'http://challenge-api.luizalabs.com/images/958ec015-cfcf-258d-c6df-1721de0ab6ea.jpg',
      brand: 'b\u00e9b\u00e9 confort',
      id: '2',
      title: 'Mois\u00e9s Dorel Windoo 1529',
    },
    {
      price: 1149.0,
      image:
        'http://challenge-api.luizalabs.com/images/6a512e6c-6627-d286-5d18-583558359ab6.jpg',
      brand: 'b\u00e9b\u00e9 confort',
      id: '3',
      title: 'Mois\u00e9s Dorel Windoo 1529',
    },
    {
      price: 1999.0,
      image:
        'http://challenge-api.luizalabs.com/images/4bd442b1-4a7d-2475-be97-a7b22a08a024.jpg',
      brand: 'b\u00e9b\u00e9 confort',
      id: '4',
      title: 'Cadeira para Auto Axiss B\u00e9b\u00e9 Confort Robin Red',
    },
  ];

  async findProduto(id: string): Promise<Produto> {
    return Promise.resolve(this.produtos.find((p) => p.id === id));
  }
}

describe('ClienteService', () => {
  let service: ClienteService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ClienteService,
        {
          provide: 'ClienteRepository',
          useClass: MongoRepositoryMock,
        },
        {
          provide: ProdutoService,
          useClass: ProdutoServiceMock,
        },
      ],
    }).compile();

    service = module.get<ClienteService>(ClienteService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('Deve criar um novo usuário', async () => {
    const novoUsuario: CreateClienteDto = {
      nome: 'novo',
      email: 'novo@novo.com',
    };

    const result = await service.create(novoUsuario);
    expect(result).toBeDefined();
    expect(result).toHaveProperty('id');
    expect(result).toHaveProperty('nome');
    expect(result).toHaveProperty('email');

    expect(result['id']).toBe('999');
    expect(result['nome']).toBe('novo');
    expect(result['email']).toBe('novo@novo.com');
  });

  it('Deve retornar todos os clientes', async () => {
    const result = await service.findAll();
    expect(result).toBeDefined();
    expect(Array.isArray(result)).toBe(true);
  });

  it('Deve retornar um cliente se for passado um id válido', async () => {
    const ID = 'e9a72482-7e95-44ff-ea5a-75147aef2181';
    const result = await service.findOne(ID);

    expect(result).toBeDefined();
    expect(result).toHaveProperty('id');
    expect(result['id']).toBe(ID);
  });

  it('Deve retornar nulo se for passado um id inválido', async () => {
    const ID = 'e';
    const result = await service.findOne(ID);

    expect(result).toBeUndefined();
  });

  it('deve atualizar um cliente quando é passado um id válido', async () => {
    const atualizacaoCliente = {
      id: 'e9a72482-7e95-44ff-ea5a-75147aef2181',
      nome: 'Teste9',
      email: 'teste9@teste.com',
      favoritos: [],
    };

    const result = await service.update(
      atualizacaoCliente.id,
      atualizacaoCliente,
    );

    expect(result).toBeDefined();
    expect(result).toHaveProperty('nome');
    expect(result['nome']).toBe(atualizacaoCliente.nome);
  });

  it('deve retornar erro quando é passado um id inválido', async () => {
    const atualizacaoCliente = {
      id: '1',
      nome: 'Teste9',
      email: 'teste9@teste.com',
      favoritos: [],
    };

    await expect(
      service.update(atualizacaoCliente.id, atualizacaoCliente),
    ).rejects.toEqual(new NotFoundException());
  });

  it('deve remover um cliente quando é passado um id válido', async () => {
    const ID = 'e9a72482-7e95-44ff-ea5a-75147aef2181';

    await service.remove(ID);
    const result = await service.findAll();

    expect(result.length).toBe(2);
  });

  it('deve retornar erro quando é um passado um id inválido ao remover', async () => {
    const ID = '1';

    await expect(service.remove(ID)).rejects.toEqual(new NotFoundException());
  });

  it('deve adicionar produto quando é passado um id válido', async () => {
    const ID_CLIENTE = 'e9a72482-7e95-44ff-ea5a-75147aef2181';
    const PRODUTO: Produto = {
      price: 1699.0,
      image:
        'http://challenge-api.luizalabs.com/images/1bf0f365-fbdd-4e21-9786-da459d78dd1f.jpg',
      brand: 'b\u00e9b\u00e9 confort',
      id: '1',
      title: 'Cadeira para Auto Iseos B\u00e9b\u00e9 Confort Earth Brown',
    };

    await service.adicionarProduto(ID_CLIENTE, PRODUTO);

    const cliente = await service.findOne(ID_CLIENTE);

    expect(cliente).toBeDefined();
    expect(cliente).toHaveProperty('favoritos');
    expect(cliente['favoritos']).toHaveLength(1);
  });

  it('deve retornar erro quando o id é inválido', async () => {
    const ID_CLIENTE = '1';
    const PRODUTO: Produto = {
      price: 1699.0,
      image:
        'http://challenge-api.luizalabs.com/images/1bf0f365-fbdd-4e21-9786-da459d78dd1f.jpg',
      brand: 'b\u00e9b\u00e9 confort',
      id: '1',
      title: 'Cadeira para Auto Iseos B\u00e9b\u00e9 Confort Earth Brown',
    };

    await expect(service.adicionarProduto(ID_CLIENTE, PRODUTO)).rejects.toEqual(
      new NotFoundException(),
    );
  });

  it('deve remover o produto da lista de favoritos do cliente quando é passado um id válido', async () => {
    const ID_CLIENTE = 'e9a72482-7e95-44ff-ea5a-75147aef2181';
    const ID_PRODUTO = '1';

    const PRODUTO: Produto = {
      price: 1699.0,
      image:
        'http://challenge-api.luizalabs.com/images/1bf0f365-fbdd-4e21-9786-da459d78dd1f.jpg',
      brand: 'b\u00e9b\u00e9 confort',
      id: '1',
      title: 'Cadeira para Auto Iseos B\u00e9b\u00e9 Confort Earth Brown',
    };

    await service.adicionarProduto(ID_CLIENTE, PRODUTO);
    await service.removerProduto(ID_CLIENTE, ID_PRODUTO);
    const cliente = await service.findOne(ID_CLIENTE);

    expect(cliente).toBeDefined();
    expect(cliente).toHaveProperty('favoritos');
    expect(cliente['favoritos']).toHaveLength(0);
  });

  it('deve retornar erro quando é a passado um id inválido ao remover produto', async () => {
    const ID_CLIENTE = '1';
    const ID_PRODUTO = '1';

    await expect(
      service.removerProduto(ID_CLIENTE, ID_PRODUTO),
    ).rejects.toEqual(new NotFoundException());
  });
});
